Thank you for filing a feature request for track.easterbunny.cc. If you want to have a feature you're considering added to the tracker, make sure you give us as much detail as possible for your proposal.

## Checklist
- [ ] I've made sure that the tracker doesn't already have a feature similar to what I'm proposing
- [ ] I've made sure that this feature relates to the tracker.
- [ ] I've made sure that I'm only requesting tracker features. Requests for games, etc are not allowed.
- [ ] I've read the code of conduct
- [ ] This is not a security related issue, and can be publicly disclosed.

## Feature description
Using as much detail as possible, please describe the feature you want to add.

## Feature workflow
Using as much detail as possible, please describe the workflow of the feature you want to add.

## Feature mockups
Using as much detail as possible, please provide some mockups (sketches) of what you envision the feature to look like.
