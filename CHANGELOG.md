# track.easterbunny.cc Changelog
This changelog details all the changes made to track.easterbunny.cc made over time on the master branch, since v4.5.4.

## Version 4.5.4 / Geo API Version 1.3.0 / Route Compiler Version 1.0.0 / Twitter Bot Version 1.0.0 - Released on May 10, 2020
* We're open source!
* The Terms of Service are no longer in effect as of May 10th, 2020.
* The about window in the tracker, in addition to other pages have been updated to reflect that the tracker is open source.
* The FAQ page has been updated with some FAQs about the tracker being open source.
* All page titles have been updated to Easter Bunny Tracker 2021.